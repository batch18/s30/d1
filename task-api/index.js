const express = require('express');
const mongoose = require('mongoose'); //this code is to be used on our db connection and to create our schema and model for our existing MongoDB atlast collection
const app = express(); //creating a server through the use of app
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//mongoose.connect - is a way to connect our mongodb atlas db connection string to our server
//paste inside the connect() method the connection string copied from the mongodb atlas db, it must be enclosed with double/single/backticks qoute
//remember to replace the password and db name with their actual values
//Due to updates made by MongoDB Atlas developers, the default connection string is being flagged as an error, to skip that error or warning that we are going to encounter in the future, we will the useNewUrlParser and useUnifiedTopology objects inside our mongoose.connect
mongoose.connect("mongodb+srv://LaFenice:_3Valentine-4@cluster0.znk4w.mongodb.net/b125-tasks?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
).then(()=> { //if the mongoose suceeded on the connection, then we will console.log message
	console.log("Successfully Connected to Database!");
}).catch((error)=> { //handles error when the mongoose failed to connect on our mongodb atlas database
	console.log(error);
});

/* ---------------------------------------------------------------------------------------- */

/*Schema - gives a structure of what kind of record/document we are going to contain on our database*/
// Schema() method - determines the structure of the documents to be written in the database
// Schema acts as blueprint to our data
// We used the Schema() constructor of the Mongoose dependency to create a new Schema object for our tasks collection
// The "new" keyword, creates a new Schema
const taskSchema = new mongoose.Schema({
	//Define the fields with their corresponding data type
	//For task, it needs a field called 'name' and 'status'
	// The field name has a data type of 'String'
	// The field status has a data type of 'Boolean' with a default value of 'false'
	name: String,
	status: {
		type: Boolean,
		//Default values are the predefined values for a field if we don't put any value
		default: false
	}
});

/*  to perform the CRUD operation for our defined collections with corresponding schema*/
//The Task variable will contain the model for our tasks collection that and shall perform the CRUD operations
//The first parameter of the mongoose.model method indicates the collection in where to store the data. Take note: the collection name must be written in singular form and the first letter of the name must in uppercase
//The second parameter is used to specify the Schema/Blueprint of the documents that will be stored on the tasks collection
const Task = mongoose.model('Task', taskSchema);


/*
	Business Logic - Todo List application
		- CRUD operation for our tasks collection
*/
// insert new task
app.post('/add-task', function(req, res){
	// call the model for our task collection
	// create an instance of the task model and save it to our database
	// creating a new task with a task name 'PM Break' through the use of task model
	let newTask = new Task({
		name: req.body.taskName
	});
	//telling our server that the newTask will now be saved as a new document to our task collection on our database 
	// .save() - saves a new document to our db
	// our callback will recieve 2 values, the error and the saved document
	// error value shall contain the error whenever there is an error encountered while we are saving our document
	// savedTask shall contrain the newly saved document from the database once the saving process is successful
	newTask.save((error, savedTask)=>{
		if (error) {
			console.log(error);
		}
		else {
			res.send(`New task saved! ${savedTask}`);
		}

	});
})

app.get('/retrieve-tasks', (req, res)=> {
	// find({}) will retrieve all the documents from the tasks collection 
	// the error on the callback will handle the errors encountered while retrieve the records 
	// the records on the callback will handle the raw data from the database
	Task.find({}, (error, records) => {
		if(error){
			console.log(error);
		}
		else {
			res.send(records);
		}
	});
});

app.get('/retrieve-tasks-done', (req, res)=> {
	Task.find({status:true}, (error, records) => {
		if(error){
			console.log(error);
		}
		else {
			res.send(records);
		}
	});
});

app.put('/complete-task/:taskId', (req, res)=> {
	//res.send({ urlParams: req.params.taskId });
	let taskId = req.params.taskId
	Task.findByIdAndUpdate(taskId, {status:true}, (error, updatedTask) => {
		if(error){
			console.log(error);
		}
		else {
			res.send('task complete Successfully');
		}
	});
});

app.delete('/delete-task/:taskId', (req, res)=> {
	//res.send({ urlParams: req.params.taskId });
	let taskId = req.params.taskId
	Task.findByIdAndDelete(taskId, (error, updatedTask) => {
		if(error){
			console.log(error);
		}
		else {
			res.send('task deleted Successfully');
		}
	});
});


app.listen(port, ()=> console.log(`Server is running at port ${port}`));




/*Mini - activity*/

const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	userName: String,
	password: String
});
const User = mongoose.model('User', userSchema);


/*Mini - activity*/
app.post('/register', function(req, res){
	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		userName: req.body.userName,
		password: req.body.password
	});
	newUser.save((error, registeredUser)=>{
		if (error) {
			console.log(error);
		}
		else {
			res.send(`Successfully registered: ${registeredUser}`);
		}

	});
})

app.get('/retrieve-users', (req, res)=> {
	User.find({}, (error, users) => {
		if(error){
			console.log(error);
		}
		else {
			res.send(users);
		}
	});
});

